package com.example.ehotel.food;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ehotel.R;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;

public class CartRecyclerViewAdapter extends RecyclerView.Adapter<CartRecyclerViewAdapter.CardViewHolder> implements  View.OnClickListener{

    public Button button_remove;
    HashMap<MenuItem, Integer> cartItems;

    @Override
    public void onClick(View v) {
        if(v.equals(button_remove)){
            removeAt(v.getVerticalScrollbarPosition());
            //}else if (mItemClickListener != null) {
            //    mItemClickListener.onItemClick(v, getPosition());
        }
    }

    public void removeAt(int position) {
        cartItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cartItems.size());
    }


    public static class CardViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView textName;
        TextView textPrice;
        ImageView image;
        Button button_remove;
        TextView textCounter;

        CardViewHolder (View itemView) {
            super(itemView);
            //button_remove.setOnClickListener(CartRecyclerViewAdapter.this);
            //itemView.setOnClickListener(itemView.this);
            button_remove = (Button)itemView.findViewById(R.id.button_remove);
            textCounter = (TextView)itemView.findViewById(R.id.textView_counter);
            //cardView = (CardView)itemView.findViewById(R.id.it);
            textName = (TextView) itemView.findViewById(R.id.textView_name);
            textPrice = (TextView)itemView.findViewById(R.id.textView_price);
            image = (ImageView)itemView.findViewById(R.id.card_image);

            //button_remove.setOnClickListener((View.OnClickListener) this);
        }
    }

    CartRecyclerViewAdapter(HashMap<MenuItem, Integer> cartItems)
    {
        this.cartItems = cartItems;
    }

    @Override
    public int getItemCount()
    {
        return cartItems.size();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_card, viewGroup, false);
        CardViewHolder cardViewHolder = new CardViewHolder(v);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int i)
    {
        List<MenuItem> positions = new ArrayList<MenuItem>();

        for(MenuItem menuItem: cartItems.keySet())
        {
            positions.add(menuItem);
        }
        cardViewHolder.textName.setText(positions.get(i).name);
        cardViewHolder.textPrice.setText(positions.get(i).price);
        cardViewHolder.image.setImageDrawable(positions.get(i).image);
        cardViewHolder.textCounter.setText(cartItems.get(positions.get(i)).toString());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

}