package com.example.ehotel.food;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.ehotel.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RestaurantActivity extends AppCompatActivity {

    RecyclerView recycler;
    RecyclerView cart;

    Button buttonConfirm;

    LinearLayoutManager layoutManager;
    private LinearLayoutManager horizontalLinearLayoutManagerMenu;
    private LinearLayoutManager horizontalLinearLayoutManagerCart;

    List<MenuItem> menuItems;
    List<MenuItem> cartItems;

    HashMap<MenuItem, Integer> orderMap = new HashMap<MenuItem, Integer>();
    ArrayList<MenuItemForBundle> orderList = new ArrayList<MenuItemForBundle>();

    MenuLoader menuLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        menuLoader = new MenuLoader(getApplicationContext());

        menuItems = new ArrayList<>();
        cartItems = new ArrayList<>();

        buttonConfirm = (Button)findViewById(R.id.button_check_out);

        checkBtn();

        recycler = (RecyclerView)findViewById(R.id.recycler_menu);
        cart = (RecyclerView)findViewById(R.id.recycler_cart);

        layoutManager = new LinearLayoutManager(this);
        horizontalLinearLayoutManagerMenu = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        horizontalLinearLayoutManagerCart = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        recycler.setLayoutManager(horizontalLinearLayoutManagerMenu);
        cart.setLayoutManager(horizontalLinearLayoutManagerCart);

        loadMenuFromMemoryOrDownload();
        //RecyclerViewAdapter adapter = new RecyclerViewAdapter(menuItems);
        //new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recycler);
        //recycler.setAdapter(adapter);
    }

    //Menu swipe
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.DOWN) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //cartItems.add(menuItems.get(viewHolder.getAdapterPosition()));
                //pseudoAddToCart(menuItems.get(viewHolder.getAdapterPosition()));
                AddToOrder(menuItems.get(viewHolder.getAdapterPosition()));


                cartItems.clear();
                for(MenuItem menuItem: orderMap.keySet())
                {
                    cartItems.add(menuItem);
                }

                CartRecyclerViewAdapter cartAdapter = new CartRecyclerViewAdapter(orderMap);
                new ItemTouchHelper(itemTouchHelperCallbackCart).attachToRecyclerView(cart);
                cart.setAdapter(cartAdapter); //TODO move out

                RecyclerViewAdapter adapter = new RecyclerViewAdapter(menuItems);
                recycler.setAdapter(adapter);

                checkBtn();
        }
    };

    //Cart swipe
    ItemTouchHelper.SimpleCallback itemTouchHelperCallbackCart = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.UP) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            //cartItems.remove(cartItems.get(viewHolder.getAdapterPosition()));
            //pseudoRemoveFromCart(cartItems.get(viewHolder.getAdapterPosition()));
            RemoveFromOrder(cartItems.get(viewHolder.getAdapterPosition()));

            cartItems.clear();
            for(MenuItem menuItem: orderMap.keySet())
            {
                cartItems.add(menuItem);
            }

            CartRecyclerViewAdapter cartAdapter = new CartRecyclerViewAdapter(orderMap);
            new ItemTouchHelper(itemTouchHelperCallbackCart).attachToRecyclerView(cart);
            cart.setAdapter(cartAdapter);

            checkBtn();
        }
    };

    //Buttons
    public void button_card_delete(View v)
    {

    }

    public void button_confirm(View v)
    {
        Gson gson = new Gson();
        if(!orderMap.isEmpty()) {

            for (Map.Entry<MenuItem, Integer> el: orderMap.entrySet())
            {
                orderList.add(new MenuItemForBundle(el.getKey().name, el.getKey().description, Double.parseDouble(el.getKey().price), el.getValue()));
            }

            Intent orderIntent = new Intent(getApplicationContext(), Bill.class);
            orderIntent.putParcelableArrayListExtra("OrderList", orderList);

            startActivity(orderIntent);
        }
    }

    public void loadMenuFromMemoryOrDownload()
    {
        File f = new File("/data/data/com.example.ehotel/shared_prefs/file.xml");

        if (f.exists()) {

            String json = MenuLoader.loadFromSharedPreferences("file", "menu", getApplicationContext());
            Gson gson = new Gson();
            menuItems = gson.fromJson(json, new TypeToken<ArrayList<MenuItem>>() {
            }.getType());
            for (MenuItem el : menuItems) {
                el.image = MenuLoader.loadImageFromStorage(el.imgPath, el.name);
            }

            RecyclerViewAdapter adapter = new RecyclerViewAdapter(menuItems);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recycler);
            recycler.setAdapter(adapter);

        } else {
            MenuLoader.readData(new MenuLoader.MyCallback() {
                @Override
                public void onCallback(ArrayList<MenuItem> menuList) {



                    String menuJsn = MenuLoader.menuListToJson(menuList);

                    MenuLoader.saveToSharedPreferences(menuJsn, "file", "menu", getBaseContext());

                    String json = MenuLoader.loadFromSharedPreferences("file", "menu", getApplicationContext());
                    Gson gson = new Gson();
                    menuItems = gson.fromJson(json, new TypeToken<ArrayList<MenuItem>>() {
                    }.getType());
                    for (MenuItem el : menuItems) {
                        el.image = MenuLoader.loadImageFromStorage(el.imgPath, el.name);
                    }

                    RecyclerViewAdapter adapter = new RecyclerViewAdapter(menuItems);
                    new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recycler);
                    recycler.setAdapter(adapter);
                }
            });
        }
    }


    public void checkBtn()
    {
        if (orderMap.isEmpty())
        {
            buttonConfirm.setEnabled(false);
        }
        else
        {
            buttonConfirm.setEnabled(true);
        }
    }

    public void AddToOrder(MenuItem menuItem)
    {
        if(orderMap.containsKey(menuItem))
        {
            orderMap.put(menuItem, orderMap.get(menuItem)+1);
        }else
        {
            orderMap.put(menuItem, 1);
        }
    }

    public void RemoveFromOrder(MenuItem menuItem)
    {
        if(orderMap.containsKey(menuItem))
        {
            if(orderMap.get(menuItem)>1)
            {
                orderMap.put(menuItem, orderMap.get(menuItem) - 1);
            }else
            {
                orderMap.remove(menuItem);
            }
        }
    }

}