package com.example.ehotel.food;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class MenuItem {
    public String name;
    public String description;
    public String price;
    public String imgPath;
    Drawable image;

    //Integer amount;

    MenuItem()
    {
        this.name = null;
        this.description = null;
        this.price = null;
        this.imgPath = null;
        this.image = null;
        //this.amount = null;
    }

    MenuItem(String name, String description, String price, String imgPath, Drawable image)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imgPath = imgPath;
        this.image = image;
        //this.amount = amount;
    }

    MenuItem(String name, String description, String price, String imgPath)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imgPath = imgPath;
        //this.image = image;
        //this.amount = 1;
    }
}