package com.example.ehotel.food;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

public class MenuItemForBundle implements Parcelable {

    String name;
    String description;
    Double price;
    int amount;

    public MenuItemForBundle()
    {
        this.name = null;
        this.description = null;
        this.price = null;
        this.amount = 0;
    }

    public MenuItemForBundle(String name, String description, Double price, int amount)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.amount = amount;
    }

    public MenuItemForBundle(String name, Double price, int amount)
    {
        this.name = name;
        this.description = "";
        this.price = price;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    protected MenuItemForBundle(Parcel in) {
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        amount = in.readInt();
    }

    public static final Creator<MenuItemForBundle> CREATOR = new Creator<MenuItemForBundle>() {
        @Override
        public MenuItemForBundle createFromParcel(Parcel in) {
            return new MenuItemForBundle(in);
        }

        @Override
        public MenuItemForBundle[] newArray(int size) {
            return new MenuItemForBundle[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeDouble(price);
        parcel.writeInt(amount);
    }
}