package com.example.ehotel.food;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.ehotel.R;

public class RestOrLobbyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_or_lobby);
    }

    public void onClickBtnRestaurant(View v)
    {
        Intent restaurantAcivityIntent = new Intent(getApplicationContext(), RestaurantTopMenuActivity.class);
        startActivity(restaurantAcivityIntent);
    }

    public void onClickBtnBar(View v)
    {
        Intent barAcivityIntent = new Intent(getApplicationContext(), RestOrLobbyActivity.class);
        startActivity(barAcivityIntent);
    }
}
