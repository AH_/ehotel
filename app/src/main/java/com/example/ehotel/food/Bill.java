package com.example.ehotel.food;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ehotel.MenuMain;
import com.example.ehotel.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Bill extends AppCompatActivity {

    DatabaseReference realtimeDatabase = FirebaseDatabase.getInstance().getReference();
    HashMap<MenuItem, Integer> orderMap = new HashMap<MenuItem, Integer>();
    ArrayList<MenuItemForBundle> orderList = new ArrayList<MenuItemForBundle>();
    String order = "";
    Date orderTime = new Date();

    ListView listViewBill;
    BillAdapter adapter;
    TextView textViewTotalValue;
    TextView textViewTotelValueEur;

    RecyclerView timePickerHoursRecyclerView;
    RecyclerView timePickerMinutesRecyclerView;

    TimePickerHoursAdapter hoursAdapter;
    TimePickerMinutesAdapter minutesAdapter;

    String hour;
    String minute;

    Boolean hoursScrolling = false, minutesScrolling = false;

    //Timer

    public class HourViewHolder extends RecyclerView.ViewHolder
    {
        private TextView textViewHour;

        public HourViewHolder(View v)
        {
            super(v);
            textViewHour = (TextView)v.findViewById(R.id.textView_hours);
        }

        public void bindHours(String hour)
        {
            textViewHour.setText(hour);
        }
    }
    private class TimePickerHoursAdapter extends RecyclerView.Adapter<HourViewHolder>
    {
        private  List<String> mHours;

        public  TimePickerHoursAdapter(List<String> hours)
        {
            mHours = hours;
        }

        @NonNull
        @Override
        public HourViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.time_picker_hours, parent, false);
            return new HourViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull HourViewHolder hourViewHolder, int position) {
            String hour = mHours.get(position);
            hourViewHolder.bindHours(hour);
        }

        @Override
        public int getItemCount() {
            return mHours.size();
            //return mHours == null ? 0 : mHours.size() * 2;
        }

    }

    public class MinutesViewHolder extends RecyclerView.ViewHolder
    {
        private TextView textViewMinute;

        public MinutesViewHolder(View v)
        {
            super(v);
            textViewMinute = (TextView)v.findViewById(R.id.textView_minutes);
        }

        public void bindMinutes(String minute)
        {
            textViewMinute.setText(minute);
        }
    }
    private class TimePickerMinutesAdapter extends RecyclerView.Adapter<MinutesViewHolder>
    {
        private  List<String> mMinutes;

        public  TimePickerMinutesAdapter(List<String> minutes)
        {
            mMinutes = minutes;
        }

        @NonNull
        @Override
        public MinutesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.time_picker_minutes, parent, false);
            return new MinutesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MinutesViewHolder minuteViewHolder, int position) {
            String minute = mMinutes.get(position);
            minuteViewHolder.bindMinutes(minute);
        }

        @Override
        public int getItemCount() {
            return mMinutes.size();

        }
    }
    //Timer

    //SingleDateAndTimePicker timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);

        Button sendButton = (Button)findViewById(R.id.button_order);
        listViewBill = (ListView)findViewById(R.id.listview_bill);
        textViewTotalValue = (TextView)findViewById(R.id.textView_total_value);
        textViewTotelValueEur = (TextView)findViewById(R.id.textView_total_value_eur);

        timePickerHoursRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_hours);
        timePickerMinutesRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_minutes);

        //Hours
        ArrayList<String> hours = new ArrayList<>();
        hours.add("22");
        hours.add("23");
        hours.add("Right");
        for(int i = 0; i<24; i++)
        {
            hours.add(String.format("%02d", i));
        }
        hours.add("Right");
        for(int i = 0; i<24; i++)
        {
            hours.add(String.format("%02d", i));
        }
        hours.add("Right");
        hours.add("00");
        hours.add("01");


        //Minutes
        ArrayList<String> minutes = new ArrayList<>();
        minutes.add("58");
        minutes.add("59");
        minutes.add("now");
        for(int i = 0; i<60; i++)
        {
            minutes.add(String.format("%02d", i));
        }
        minutes.add("now");
        for(int i = 0; i<60; i++)
        {
            minutes.add(String.format("%02d", i));
        }
        minutes.add("now");
        minutes.add("00");
        minutes.add("01");

        LinearLayoutManager linearLayoutManagerHours = new LinearLayoutManager(this);
        timePickerHoursRecyclerView.setLayoutManager(linearLayoutManagerHours);
        linearLayoutManagerHours.scrollToPosition(25); //Preset recycler view
        timePickerHoursRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstItemVisible = linearLayoutManagerHours.findFirstVisibleItemPosition();
                int lastItemVisible = linearLayoutManagerHours.findLastVisibleItemPosition();
                hour = hours.get(firstItemVisible + 2);
                if(firstItemVisible == 0)
                {
                    recyclerView.getLayoutManager().scrollToPosition(30);
                }
                if(lastItemVisible == 54)
                {
                    recyclerView.getLayoutManager().scrollToPosition(24);
                }
            }
        });

        LinearLayoutManager linearLayoutManagerMinutes = new LinearLayoutManager(this);
        timePickerMinutesRecyclerView.setLayoutManager(linearLayoutManagerMinutes);
        linearLayoutManagerMinutes.scrollToPosition(61);
        timePickerMinutesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstItemVisible = linearLayoutManagerMinutes.findFirstVisibleItemPosition();
                int lastItemVisible = linearLayoutManagerMinutes.findLastVisibleItemPosition();
                minute = minutes.get(firstItemVisible + 2);
                if(firstItemVisible == 0)
                {
                    recyclerView.getLayoutManager().scrollToPosition(66);
                }
                if(lastItemVisible == 125)
                {
                    recyclerView.getLayoutManager().scrollToPosition(59);
                }
            }
        });

        hoursAdapter = new TimePickerHoursAdapter(hours);
        minutesAdapter = new TimePickerMinutesAdapter(minutes);

        LinearSnapHelper snapHelperHours = new LinearSnapHelper();
        LinearSnapHelper snapHelperMinutes = new LinearSnapHelper();
        snapHelperHours.attachToRecyclerView(timePickerHoursRecyclerView);
        snapHelperMinutes.attachToRecyclerView(timePickerMinutesRecyclerView);

        timePickerHoursRecyclerView.setAdapter(hoursAdapter);
        timePickerMinutesRecyclerView.setAdapter(minutesAdapter);

        orderList = getIntent().getParcelableArrayListExtra("OrderList");

        for(MenuItemForBundle el : orderList)
        {
            order += el.name+" x"+el.amount+"|";
        }
        order += " at " + hour + ":" + minute;

        adapter = new BillAdapter(Bill.this, R.layout.bill_row, orderList);
        listViewBill.setAdapter(adapter);

        double sum = 0;

        for (MenuItemForBundle el: orderList) {
            sum += el.amount * el.price;
        }

        textViewTotalValue.setText(sum + " Kč");
        textViewTotelValueEur.setText(sum/25 + " Eur");
    }

    public void onClickBtnSendOrder(View v)
    {
        realtimeDatabase.child("hotels").child("property").child("rooms").child("0").child("orders").setValue(order);

        Intent backToMainIntent = new Intent(getApplicationContext(), MenuMain.class);
        String orderSent = "orderSent";
        backToMainIntent.putExtra("orderStatus", orderSent);

        startActivity(backToMainIntent);
    }

    public void onClickBack(View v)
    {
        super.onBackPressed();
    }
}
