package com.example.ehotel.food;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class MenuLoader extends Application {

    ArrayList<MenuItem> loadedMenuItemsCrutch = new ArrayList<MenuItem>();

    public static Context context;

    public static FirebaseDatabase mDatabase;
    public static DatabaseReference mDatabaseReference;

    public static FirebaseStorage mFirebaseStorage;

    private static ArrayList<MenuItem> menuItems;

    private static String menuJson;

    public MenuLoader(Context context)
    {
        this.context = context;
        mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference().child("hotels").child("property").child("restaurant").child("menu");

        mFirebaseStorage = FirebaseStorage.getInstance();

        menuItems = new ArrayList<>();
    }

    public interface MyCallback
    {
        void onCallback(ArrayList<MenuItem> menuList);
    }

    public void downloadRestaurantMenuFromFirebase()
    {
        readData(new MyCallback() {
            @Override
            public void onCallback(ArrayList<MenuItem> menuList) {
                String menuJsn = menuListToJson(menuList);
                //saveToInternalStorage(menuJsn, "menu");
                saveToSharedPreferences(menuJsn, "file", "menu", context);
            }
        });
    }

    public ArrayList<MenuItem> downloadRestaurantMenuFromFirebaseAndReturn()
    {
        readData(new MyCallback() {
            @Override
            public void onCallback(ArrayList<MenuItem> menuList) {
                String menuJsn = menuListToJson(menuList);
                //saveToInternalStorage(menuJsn, "menu");
                saveToSharedPreferences(menuJsn, "file", "menu", context);

                String json = loadFromSharedPreferences("file", "menu", context);
                Gson gson = new Gson();
                loadedMenuItemsCrutch = gson.fromJson(json, new TypeToken<ArrayList<MenuItem>>() {
                }.getType());
                for (MenuItem el : loadedMenuItemsCrutch) {
                    el.image = loadImageFromStorage(el.imgPath, el.name);
                }

            }
        });

        ArrayList<MenuItem> loadedMenuItemsCrutchInnerCrutch = loadedMenuItemsCrutch;

        return loadedMenuItemsCrutchInnerCrutch;
    }

    public static String saveToInternalStorage(Bitmap bitmapImage, String filename)
    {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,filename+".png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public void removeFromInternalStorage()
    {
        try {
            File dir = new File("/data/data/com.example.ehotel/app_imageDir");
            if (dir.isDirectory())
            {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++)
                {
                    new File(dir, children[i]).delete();
                }
            }


            /*
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            directory.delete();
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String saveToInternalStorage(String json, String filename)
    {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,filename+".txt");

        try {
            FileOutputStream f = new FileOutputStream(mypath);
            PrintWriter pw = new PrintWriter(f);
            pw.println(json);
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i(TAG, "******* File not found. Did you" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath();
    }

    public static void saveToSharedPreferences(String stringToSave, String fileKey, String valueKey, Context context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(fileKey, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(valueKey, stringToSave);
        editor.commit();
    }

    public void deleteFromSharedPreferences(String fileKey, Context context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(fileKey, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }

    public static String loadFromSharedPreferences(String preferenceFileKey, String stringKey, Context context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(preferenceFileKey, Context.MODE_PRIVATE);
        String resultString = sharedPref.getString(stringKey, "failed - defaultValue");
        return resultString;
    }

    public static Drawable loadImageFromStorage(String path, String name)
    {
        try {
            File f=new File(path, name+".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            Drawable img = new BitmapDrawable(b);
            return img;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return  null;
    }

    public static String menuListToJson(ArrayList<MenuItem> list)
    {
        JSONArray array = new JSONArray();
        for (int i =0; i<list.size() ; i++ ) {
            JSONObject object = new JSONObject();
            try {
                object.put("name", list.get(i).name);
                object.put("description", list.get(i).description);
                object.put("price", list.get(i).price);
                object.put("imgPath", list.get(i).imgPath);
                array.put(object);
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array.toString();
    }

    public static void readData(final MyCallback myCallback)
    {
        //Firebase RealtimeDatabase
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //Create array of load tasks to load images from
                ArrayList<Task> loadTasks = new ArrayList<Task>();

                for (DataSnapshot dish : dataSnapshot.getChildren())
                {
                    final MenuItem menuItem = new MenuItem();
                    menuItem.name = dish.child("name").getValue().toString();
                    menuItem.description = dish.child("description").getValue().toString();
                    menuItem.price = dish.child("price").getValue().toString();

                    //Firebase Stoarge
                    StorageReference tempReference = mFirebaseStorage.getReferenceFromUrl(dish.child("imgUrl").getValue().toString());
                    try
                    {
                        //Create single load task to load an image
                        final File theImage = File.createTempFile("img", "png");
                        Task loadTask = tempReference.getFile(theImage);
                        //Add load task to taks list
                        loadTasks.add(loadTask);

                        //Handlind single load task onSucsess
                        loadTask.addOnSuccessListener(new OnSuccessListener() {
                            @Override
                            public void onSuccess(Object o) {
                                Bitmap bitmap = BitmapFactory.decodeFile(theImage.getAbsolutePath());
                                menuItem.imgPath = saveToInternalStorage(bitmap, menuItem.name);
                                menuItems.add(menuItem);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                System.out.println("!!! Fail while loading image");
                            }
                        });

                    }catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                //When all images loaded and added to ArrayList<MenuItem>
                Task<Void> allLoads = Tasks.whenAll(loadTasks.toArray(new Task[loadTasks.size()]));
                allLoads.addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        myCallback.onCallback(menuItems);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        mDatabaseReference.addListenerForSingleValueEvent(valueEventListener);
    }

}
