package com.example.ehotel.food;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ehotel.R;
import com.example.ehotel.food.MenuItemForBundle;

import java.util.List;

public class BillAdapter extends ArrayAdapter<MenuItemForBundle> {

    private Context mContext;
    int mResorce;

    public BillAdapter(@NonNull Context context, int resource, @NonNull List<MenuItemForBundle> objects) {
        super(context, resource, objects);
        mContext = context;
        mResorce = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name = getItem(position).getName();
        Double price = getItem(position).getPrice();
        int amount = getItem(position).getAmount();

        MenuItemForBundle menuItemForBundle = new MenuItemForBundle(name, price, amount);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResorce, parent, false);

        TextView textViewName = (TextView)convertView.findViewById(R.id.textView_name);
        //TextView textViewPrice = (TextView)convertView.findViewById(R.id.textView_price);
        TextView textViewAmount = (TextView)convertView.findViewById(R.id.textView_amount);
        TextView textViewSubtotal = (TextView)convertView.findViewById(R.id.textView_subtotal);

        textViewName.setText(name);
        //textViewPrice.setText(String.valueOf(price));
        if(amount>1)
        {
            textViewAmount.setText("x" + String.valueOf(amount));
        }else
        {
            textViewAmount.setText("");
        }
        textViewSubtotal.setText(String.valueOf(amount*price) + " Kč");

        return convertView;
    }
}
