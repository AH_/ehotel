package com.example.ehotel.food;

import android.graphics.drawable.Drawable;

public class MenuItemForOrder {
    public String name;
    public String description;
    public String price;
    public String imgPath;
    public int amount;
    Drawable image;


    MenuItemForOrder()
    {
        this.name = null;
        this.description = null;
        this.price = null;
        this.imgPath = null;
        this.image = null;
        this.amount = 0;
    }

    MenuItemForOrder(String name, String description, String price, String imgPath, int amount, Drawable image)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imgPath = imgPath;
        this.image = image;
        this.amount = amount;
    }

    MenuItemForOrder(String name, String description, String price, int amount, Drawable image)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imgPath = null;
        this.image = image;
        this.amount = amount;
    }

    MenuItemForOrder(String name, String description, String price, String imgPath)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imgPath = imgPath;
        //this.image = image;
        //this.amount = 1;
    }
}