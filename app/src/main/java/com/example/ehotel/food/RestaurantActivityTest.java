package com.example.ehotel.food;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ehotel.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class RestaurantActivityTest extends Activity {

    ArrayList<MenuItem> listOfsalats = new ArrayList<MenuItem>();
    HashMap<MenuItem, Integer> orderMap = new HashMap<MenuItem, Integer>();
    HashMap<View, Integer> orderMapView = new HashMap<View, Integer>();
    ArrayList<MenuItemForBundle> orderListBundle = new ArrayList<MenuItemForBundle>();
    public static ArrayList<MenuItemForOrder> orderList = new ArrayList<MenuItemForOrder>();

    Button buttonConfirm;

    MenuLoader menuLoader;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_test);

        menuLoader  = new MenuLoader(getApplicationContext());

        buttonConfirm = (Button)findViewById(R.id.button_check_out);

        // setContentView(new MyView(this));
        loadMenuFromMemoryOrDownload();
        listSerchingMenu(listOfsalats);
        //tableOrderMenu(listOfsalats);
        //typeOfScreen();

    }

    public void listSerchingMenu(ArrayList<MenuItem> list) {
        View horizontal = (View)findViewById(R.id.horizontal_block);
        View vertikal = (View)findViewById(R.id.vertical_block);
        horizontal.setVisibility(VISIBLE);
        vertikal.setVisibility(View.GONE);

        final LinearLayout linLayout = (LinearLayout) findViewById(R.id.test_list);

        LayoutInflater ltInflater = getLayoutInflater();

        linLayout.removeAllViews();


        for (int i = 0; i < list.size(); i++) {

            Log.d("myLogs", "i = " + i);
            final View item = ltInflater.inflate(R.layout.table_line, linLayout, false);

            final LinearLayout cardButton = item.findViewById(R.id.surface_pro);

            final TextView nameOf = (TextView) item.findViewById(R.id.name_id);
            nameOf.setText(list.get(i).name);

            final TextView priceOf = (TextView) item.findViewById(R.id.price_id);
            priceOf.setText(list.get(i).price);

            final TextView timeOf = (TextView) item.findViewById(R.id.time_id);
            timeOf.setText(String.valueOf(new Random().nextInt(31)));

            final TextView descriptionOf = (TextView) item.findViewById(R.id.description_id);
            descriptionOf.setText(list.get(i).description);

            final View image = (View) item.findViewById(R.id.group);
            image.setBackground(list.get(i).image);
/*
            cardButton.setOnTouchListener(new OnSwipeTouchListener(RestaurantActivityTest.this)
            {
                public void onSwipeTop() {
                    Toast.makeText(RestaurantActivityTest.this, "top", Toast.LENGTH_SHORT).show();
                }
                public void onSwipeRight() {
                    Toast.makeText(RestaurantActivityTest.this, "right", Toast.LENGTH_SHORT).show();
                }
                public void onSwipeLeft() {
                    Toast.makeText(RestaurantActivityTest.this, "left", Toast.LENGTH_SHORT).show();
                }
                public void onSwipeBottom() {
                    int xxx = 0;
                    if (orderList.size() > 0) {
                        for (int i = 0; i < orderList.size(); i++) {
                            if (orderList.get(i).name.equals(nameOf.getText().toString())) {
                                orderList.set(i, orderList.get(i)).amount = orderList.get(i).amount + 1;
                                xxx++;
                            }
                        }
                        if (xxx == 0) {
                            orderList.add(new MenuItemForOrder(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(),
                                    1, image.getBackground()));
                        }
                    } else {
                        orderList.add(new MenuItemForOrder(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(),
                                1, image.getBackground()));
                    }
                    System.out.println(orderList);
                    for (int i = 0; i < orderList.size(); i++) {
                        System.out.println("!!!!!!!!!! order list ---" + orderList.get(i).name + " " + orderList.get(i).amount);
                    }
                    listOrderMenu(orderList);



                }

            });
*/
            cardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("-----------onClick----" + v.getX() + "---" + v.getY() + "--name--" + nameOf.getText());
                    int xxx = 0;
                    if (orderList.size() > 0) {
                        for (int i = 0; i < orderList.size(); i++) {
                            if (orderList.get(i).name.equals(nameOf.getText().toString()))
                            {
                                orderList.set(i, orderList.get(i)).amount = orderList.get(i).amount + 1;
                                xxx++;
                            }
                        }
                        if (xxx == 0) {
                            orderList.add(new MenuItemForOrder(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(),
                                    1, image.getBackground()));
                        }
                    } else {
                        orderList.add(new MenuItemForOrder(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(),
                                1, image.getBackground()));
                    }
                    System.out.println(orderList);
                    for (int i = 0; i < orderList.size(); i++) {
                        System.out.println("!!!!!!!!!! order list ---" + orderList.get(i).name + " " + orderList.get(i).amount);
                    }
                    listOrderMenu(orderList);
                    //AddToOrder(new MenuItem(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(), null));
                    AddToOrder(v);
                }
            });

/*
            cardButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View vv) {
                    System.out.println("-----------onLongClick----" + vv.getX() + "---" + vv.getY() + "--name--" + nameOf.getText());
                    int xxx = 0;
                    if (orderList.size() > 0) {
                        for (int i = 0; i < orderList.size(); i++) {
                            if (orderList.get(i).name.equals(nameOf.getText().toString())) {
                                orderList.set(i, orderList.get(i)).amount = orderList.get(i).amount + 1;
                                xxx++;
                            }
                        }
                        if (xxx == 0) {
                            orderList.add(new MenuItemForOrder(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(),
                                    1, image.getBackground()));
                        }
                    } else {
                        orderList.add(new MenuItemForOrder(nameOf.getText().toString(), descriptionOf.getText().toString(), priceOf.getText().toString(),
                                1, image.getBackground()));
                    }
                    System.out.println(orderList);
                    for (int i = 0; i < orderList.size(); i++) {
                        System.out.println("!!!!!!!!!! order list ---" + orderList.get(i).name + " " + orderList.get(i).amount);
                    }
                    listOrderMenu(orderList);

                    // vv.setBackgroundColor(Color.parseColor("#559966CC"));
                    View x = findViewById(R.id.rectangle_2);
                    vv.startDrag(null, new View.DragShadowBuilder(vv), vv, 0);
                    System.out.println("-----------startDrag----" + vv.getX() + "---" + vv.getY());
                    vv.setOnDragListener(new View.OnDragListener() {


                        @Override
                        public boolean onDrag(View v, DragEvent event) {
                            System.out.println("----------------------drag_listener");
                            final int dragAction = event.getAction();
                            View dragView = (View) event.getLocalState();


                            switch (event.getAction()) {
                                case DragEvent.ACTION_DRAG_STARTED:
                                    System.out.println("__________start" + v.getX() + "---" + v.getY());
                                    break;
                                case DragEvent.ACTION_DRAG_ENTERED:
                                    System.out.println("__________entered");   //change the shape of the view
                                    break;

                                case DragEvent.ACTION_DRAG_EXITED:
                                    System.out.println("__________exited");   //change the shape of the view back to normal
                                    break;

                                //drag shadow has been released,the drag point is within the bounding box of the View
                                case DragEvent.ACTION_DROP:

                                    // if the view is the bottomlinear, we accept the drag item
                                    if (v == findViewById(R.id.rectangle_2)) {


                                        System.out.println("__________drpo - true");
                                    } else {

                                        System.out.println("__________drop - false");
                                        break;
                                    }
                                    break;

                                //the drag and drop operation has concluded.
                                case DragEvent.ACTION_DRAG_ENDED:
                                    System.out.println("__________ended");   //go back to normal shape

                                default:
                                    break;
                            }

                            return false;
                        }

                    });
                    return false;
                }
            });
*/

            linLayout.addView(item);
        }


        View left = (View) findViewById(R.id.left);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("--------------Left");
                linLayout.removeAllViews();
                tableOrderMenu(listOfsalats);
                View sq1 = (View) findViewById(R.id.square_1);
                View sq2 = (View) findViewById(R.id.square_2);
                View sq3 = (View) findViewById(R.id.square_3);
                View sq4 = (View) findViewById(R.id.square_4);
                View ln1 = (View) findViewById(R.id.line_1);
                View ln2 = (View) findViewById(R.id.line_2);
                View ln3 = (View) findViewById(R.id.line_3);
                sq1.setBackgroundColor(getResources().getColor(R.color.active));
                sq2.setBackgroundColor(getResources().getColor(R.color.active));
                sq3.setBackgroundColor(getResources().getColor(R.color.active));
                sq4.setBackgroundColor(getResources().getColor(R.color.active));
                ln1.setBackgroundColor(getResources().getColor(R.color.not_active));
                ln2.setBackgroundColor(getResources().getColor(R.color.not_active));
                ln3.setBackgroundColor(getResources().getColor(R.color.not_active));

            }
        });
    }


    public void tableOrderMenu(ArrayList<MenuItem> list) {
        View horizontal = (View)findViewById(R.id.horizontal_block);
        View vertikal = (View)findViewById(R.id.vertical_block);
        horizontal.setVisibility(View.GONE);
        vertikal.setVisibility(VISIBLE);

        final TableLayout tableLayout = (TableLayout) findViewById(R.id.test_list1);
        tableLayout.removeAllViews();
        //tableLayout.setStretchAllColumns(true);

        int numbersRows = list.size() / 6 + 1;
        int x = 0;
        System.out.println("----------------------------------------------------------" + numbersRows);

        for (int i = 0; i < numbersRows; i++) {
            TableRow tableRow = new TableRow(this);
            for (int j = 0; j < 6; j++) {
                if (x >= list.size()) break;
                // LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LayoutInflater layoutInflater = getLayoutInflater();
                View cell = layoutInflater.inflate(R.layout.table_table, tableRow, false);

                final TextView nameOf = (TextView) cell.findViewById(R.id.name_id);
                nameOf.setText(list.get(x).name);

                final TextView priceOf = (TextView) cell.findViewById(R.id.price_id);
                priceOf.setText(list.get(x).price);

                final TextView timeOf = (TextView) cell.findViewById(R.id.time_id);
                timeOf.setText(String.valueOf(new Random().nextInt(31)));

                final View image = (View) cell.findViewById(R.id.group);
                image.setBackground(list.get(x).image);

                x++;

                cell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println(nameOf.getText());


                        System.out.println("-----------onLongClick----" + v.getX() + "---" + v.getY() + "--name--" + nameOf.getText());
                        int xxx = 0;
                        if (orderList.size() > 0) {
                            for (int i = 0; i < orderList.size(); i++) {
                                if (orderList.get(i).name.equals(nameOf.getText().toString())) {
                                    orderList.set(i, orderList.get(i)).amount = orderList.get(i).amount + 1;
                                    xxx++;
                                }
                            }
                            if (xxx == 0) {
                                orderList.add(new MenuItemForOrder(nameOf.getText().toString(), "", priceOf.getText().toString(),
                                        1, image.getBackground()));
                            }
                        } else {
                            orderList.add(new MenuItemForOrder(nameOf.getText().toString(), "", priceOf.getText().toString(),
                                    1, image.getBackground()));
                        }
                        System.out.println(orderList);
                        for (int i = 0; i < orderList.size(); i++) {
                            System.out.println("!!!!!!!!!! order list ---" + orderList.get(i).name + " " + orderList.get(i).amount);
                        }
                        listOrderMenu(orderList);

                        // vv.setBackgroundColor(Color.parseColor("#559966CC"));
                        View x = findViewById(R.id.rectangle_2);
                        v.startDrag(null, new View.DragShadowBuilder(v), v, 0);
                        System.out.println("-----------startDrag----" + v.getX() + "---" + v.getY());
                        v.setOnDragListener(new View.OnDragListener() {

                            @Override
                            public boolean onDrag(View v, DragEvent event) {
                                System.out.println("----------------------drag_listener");
                                final int dragAction = event.getAction();
                                View dragView = (View) event.getLocalState();
                                return false;
                            }
                        });
                    }
                });
/*
                cell.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        System.out.println(nameOf.getText());


                        System.out.println("-----------onLongClick----" + v.getX() + "---" + v.getY() + "--name--" + nameOf.getText());
                        int xxx = 0;
                        if (orderList.size() > 0) {
                            for (int i = 0; i < orderList.size(); i++) {
                                if (orderList.get(i).name.equals(nameOf.getText().toString())) {
                                    orderList.set(i, orderList.get(i)).amount = orderList.get(i).amount + 1;
                                    xxx++;
                                }
                            }
                            if (xxx == 0) {
                                orderList.add(new MenuItemForOrder(nameOf.getText().toString(), "", priceOf.getText().toString(),
                                        1, image.getBackground()));
                            }
                        } else {
                            orderList.add(new MenuItemForOrder(nameOf.getText().toString(), "", priceOf.getText().toString(),
                                    1, image.getBackground()));
                        }
                        System.out.println(orderList);
                        for (int i = 0; i < orderList.size(); i++) {
                            System.out.println("!!!!!!!!!! order list ---" + orderList.get(i).name + " " + orderList.get(i).amount);
                        }
                        listOrderMenu(orderList);

                        // vv.setBackgroundColor(Color.parseColor("#559966CC"));
                        View x = findViewById(R.id.rectangle_2);
                        v.startDrag(null, new View.DragShadowBuilder(v), v, 0);
                        System.out.println("-----------startDrag----" + v.getX() + "---" + v.getY());
                        v.setOnDragListener(new View.OnDragListener() {

                            @Override
                            public boolean onDrag(View v, DragEvent event) {
                                System.out.println("----------------------drag_listener");
                                final int dragAction = event.getAction();
                                View dragView = (View) event.getLocalState();
                                return false;
                            }
                        });
                        return false;
                    }
                });
*/

                tableRow.addView(cell);
            }

            tableLayout.addView(tableRow);
        }


        View right = (View) findViewById(R.id.right);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("--------------Right");
                tableLayout.removeAllViews();
                listSerchingMenu(listOfsalats);
                View sq1 = (View) findViewById(R.id.square_1);
                View sq2 = (View) findViewById(R.id.square_2);
                View sq3 = (View) findViewById(R.id.square_3);
                View sq4 = (View) findViewById(R.id.square_4);
                View ln1 = (View) findViewById(R.id.line_1);
                View ln2 = (View) findViewById(R.id.line_2);
                View ln3 = (View) findViewById(R.id.line_3);
                sq1.setBackgroundColor(getResources().getColor(R.color.not_active));
                sq2.setBackgroundColor(getResources().getColor(R.color.not_active));
                sq3.setBackgroundColor(getResources().getColor(R.color.not_active));
                sq4.setBackgroundColor(getResources().getColor(R.color.not_active));
                ln1.setBackgroundColor(getResources().getColor(R.color.active));
                ln2.setBackgroundColor(getResources().getColor(R.color.active));
                ln3.setBackgroundColor(getResources().getColor(R.color.active));
            }
        });

    }

    public class OrderSalat {
        String name;
        int number;

        public OrderSalat() {
        }

        public OrderSalat(String name, int number) {
            this.name = name;
            this.number = number;
        }
    }


    public void listOrderMenu(ArrayList<MenuItemForOrder> list) {

        final LinearLayout linOrderLayout = (LinearLayout) findViewById(R.id.order_list);

        LayoutInflater ltInflater = getLayoutInflater();

        linOrderLayout.removeAllViews();

        for (int i = 0; i < list.size(); i++) {

            Log.d("myLogs", "i = " + i);
            final View item = ltInflater.inflate(R.layout.order_line, linOrderLayout, false);

            //final LinearLayout cardButton = item.findViewById(R.id.surface_pro);

            final TextView nameOf = (TextView) item.findViewById(R.id.name_id);
            nameOf.setText(list.get(i).name);

            View image = (View)item.findViewById(R.id.group);
            image.setBackground(list.get(i).image);

            if (list.get(i).amount > 1) {
                View numberView = (View) item.findViewById(R.id.numbers_of);
                numberView.setVisibility(VISIBLE);
                TextView numberOf = (TextView) item.findViewById(R.id.numbers_of_order);
                numberOf.setText("x" + list.get(i).amount);
            }

            linOrderLayout.addView(item);
        }
    }

    public void onClickTable(View v)
    {
        tableOrderMenu(listOfsalats);
    }

    public void onClickList(View v)
    {
        listSerchingMenu(listOfsalats);
    }

    public void typeOfScreen() {View left = (View) findViewById(R.id.left);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("--------------Left");
            }
        });



        View right = (View) findViewById(R.id.right);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("--------------Right");
            }
        });


    }


    //MINE

    public void AddToOrder(MenuItem menuItem)
    {
        if(orderMap.get(menuItem)!=null)
        {
            orderMap.put(menuItem, orderMap.get(menuItem)+1);
        }else
        {
            orderMap.put(menuItem, 1);
        }
    }

    public void AddToOrder(View v)
    {
        if(orderMapView.get(v)!=null)
        {
            orderMapView.put(v, orderMapView.get(v)+1);
        }else
        {
            orderMapView.put(v, 1);
        }
    }

    public void button_confirm(View v)
    {
        Gson gson = new Gson();
        /*
        if(!orderMap.isEmpty()) {

            for (Map.Entry<MenuItem, Integer> el: orderMap.entrySet())
            {
                orderListBundle.add(new MenuItemForBundle(el.getKey().name, el.getKey().description, Double.parseDouble(el.getKey().price), el.getValue()));
            }

            Intent orderIntent = new Intent(getApplicationContext(), Bill.class);
            orderIntent.putParcelableArrayListExtra("OrderList", orderListBundle);

            startActivity(orderIntent);
        }
        */
        if(!orderMapView.isEmpty()) {

            for (Map.Entry<View, Integer> el: orderMapView.entrySet())
            {
                TextView name = el.getKey().findViewById(R.id.name_id);
                TextView description = el.getKey().findViewById(R.id.description_id);
                TextView price = el.getKey().findViewById(R.id.price_id);

                orderListBundle.add(new MenuItemForBundle(name.getText().toString(), description.getText().toString(), Double.parseDouble(price.getText().toString()), el.getValue()));
            }

            Intent orderIntent = new Intent(getApplicationContext(), Bill.class);
            orderIntent.putParcelableArrayListExtra("OrderList", orderListBundle);

            startActivity(orderIntent);
        }
    }

    public void onClickBack(View v)
    {
        super.onBackPressed();
    }

    public void loadMenuFromMemoryOrDownload()
    {
        File f = new File("/data/data/com.example.ehotel/shared_prefs/file.xml");

        if (f.exists()) {

            String json = MenuLoader.loadFromSharedPreferences("file", "menu", getApplicationContext());
            Gson gson = new Gson();
            listOfsalats = gson.fromJson(json, new TypeToken<ArrayList<MenuItem>>() {
            }.getType());
            for (MenuItem el : listOfsalats) {
                el.image = MenuLoader.loadImageFromStorage(el.imgPath, el.name);
            }

        } else {
            MenuLoader.readData(new MenuLoader.MyCallback() {
                @Override
                public void onCallback(ArrayList<MenuItem> menuList) {

                    String menuJsn = MenuLoader.menuListToJson(menuList);

                    MenuLoader.saveToSharedPreferences(menuJsn, "file", "menu", getBaseContext());

                    String json = MenuLoader.loadFromSharedPreferences("file", "menu", getApplicationContext());
                    Gson gson = new Gson();
                    listOfsalats = gson.fromJson(json, new TypeToken<ArrayList<MenuItem>>() {
                    }.getType());
                    for (MenuItem el : listOfsalats) {
                        el.image = MenuLoader.loadImageFromStorage(el.imgPath, el.name);
                    }
                }
            });
        }
    }
}