package com.example.ehotel.support;

import android.animation.ObjectAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ehotel.Alarm;
import com.example.ehotel.R;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class AlarmRecyclerAdapter extends RecyclerView.Adapter<AlarmRecyclerAdapter.AlarmViewHolder>
{
    private ArrayList<Alarm> alarmList;

    public class AlarmViewHolder extends RecyclerView.ViewHolder
    {
        private TextView textViewTime;
        private TextView textViewDays;
        public LinearLayout frontView;

        public AlarmViewHolder(View v)
        {
            super(v);
            textViewTime = (TextView)v.findViewById(R.id.time);
            textViewDays = (TextView)v.findViewById(R.id.days);
            frontView = (LinearLayout)v.findViewById(R.id.frontCard);
        }

        public void bindAlarm(Alarm alarm)
        {
            String alarmTime = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(alarm.millisAfterMidnight),
                    TimeUnit.MILLISECONDS.toMinutes(alarm.millisAfterMidnight) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(alarm.millisAfterMidnight)));

            textViewTime.setText(alarmTime);
            textViewDays.setText("some days");
        }
    }

    public void MoveToRight(View view)
    {
        ObjectAnimator animation;

        if(view.getTranslationX() == -170)
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        }else
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 170);
        }

        animation.setDuration(400);
        animation.start();
    }

    public void MoveToLeft(View view)
    {
        ObjectAnimator animation;

        if(view.getTranslationX() == 170)
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        }else
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", -170);
        }

        animation.setDuration(300);
        animation.start();
    }

    public  AlarmRecyclerAdapter(ArrayList<Alarm> alarmList)
    {
        this.alarmList = alarmList;
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.alarm_card, parent, false);
        return new AlarmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder alarmViewHolder, int position) {
        Alarm alarm = alarmList.get(position);
        alarmViewHolder.bindAlarm(alarm);
    }

    @Override
    public int getItemCount() {
        return alarmList.size();
        //return mHours == null ? 0 : mHours.size() * 2;
    }

}