package com.example.ehotel.support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ehotel.Alarm;
import com.example.ehotel.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class AlarmKostylRecyclerAdapter extends RecyclerView.Adapter<AlarmKostylRecyclerAdapter.AlarmViewHolder>
{
    private ArrayList<Alarm> alarmList;
    public Context context;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(View v, int position);
        void onSwitchClick(View v, int position, boolean isChecked);
        void onDeleteClick(View v, int position);
    }



    public void SetOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public class AlarmViewHolder extends RecyclerView.ViewHolder
    {
        private TextView textViewTime;
        private TextView textViewDays;
        public LinearLayout frontView;
        public Button closeButton;
        public Switch readySwitch;

        public AlarmViewHolder(View v, OnItemClickListener listener)
        {
            super(v);
            textViewTime = (TextView)v.findViewById(R.id.time);
            textViewDays = (TextView)v.findViewById(R.id.days);
            frontView = (LinearLayout)v.findViewById(R.id.frontCard);
            closeButton = (Button)v.findViewById(R.id.button_close);
            readySwitch = (Switch)v.findViewById(R.id.switch1);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION)
                        {
                            listener.onItemClick(v, position);
                        }
                    }
                }
            });

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION)
                        {
                            listener.onDeleteClick(v, position);
                        }
                    }
                }
            });

            readySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onSwitchClick(v, position, isChecked);
                        }
                    }
                }
            });
        }

        public void bindAlarm(Alarm alarm)
        {

            String alarmTime = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(alarm.millisAfterMidnight),
                    TimeUnit.MILLISECONDS.toMinutes(alarm.millisAfterMidnight) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(alarm.millisAfterMidnight)));

            textViewTime.setText(alarmTime);
            textViewDays.setText(getDaysString(alarm));
            readySwitch.setChecked(alarm.settedUp);
        }
    }

    public AlarmKostylRecyclerAdapter(ArrayList<Alarm> alarmList, Context context)
    {
        this.alarmList = alarmList;
        this.context = context;
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.alarm_card_kostyl, parent, false);
        return new AlarmViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder alarmViewHolder, int position) {
        Alarm alarm = alarmList.get(position);
        alarmViewHolder.bindAlarm(alarm);
    }

    @Override
    public int getItemCount() {
        return alarmList.size();
        //return mHours == null ? 0 : mHours.size() * 2;
    }

    public Alarm getItem(int position)
    {
        if(alarmList.size()>0)
        {
            return alarmList.get(position);
        }else
        {
            return null;
        }
    }

    public String getDaysString(Alarm alarm)
    {
        String days = "";
        for(int i=0; i<alarm.days.length; i++)
        {
            if(alarm.days[i])
            {
                switch (i)
                {
                    case(0):days += "MON  ";break;
                    case(1):days += "TUE  ";break;
                    case(2):days += "WED  ";break;
                    case(3):days += "THU  ";break;
                    case(4):days += "FRI  ";break;
                    case(5):days += "SAT  ";break;
                    case(6):days += "SUN  ";break;
                    default:days += "WTF?";break;
                }
            }
        }
        return days;
    }

}