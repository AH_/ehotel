package com.example.ehotel.support;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ehotel.Alarm;
import com.example.ehotel.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class AlarmListAdapter extends ArrayAdapter<Alarm> {
    private final Context context;
    private final ArrayList<Alarm> values;
    AdapterCallback callback;

    public interface AdapterCallback{
        void onItemClicked(int position);
    }

    public AlarmListAdapter(Context context, ArrayList<Alarm> values, AdapterCallback callback) {
        super(context, R.layout.alarm_row, values);
        this.context = context;
        this.values = values;
        this.callback = callback;
    }

    @Override
    public Alarm getItem(int position) {
        return values.get(position);
    }

    public void MoveToRight(View view)
    {
        ObjectAnimator animation;

        if(view.getTranslationX() == -170)
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        }else
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 170);
        }

        animation.setDuration(400);
        animation.start();
    }

    public void MoveToLeft(View view)
    {
        ObjectAnimator animation;

        if(view.getTranslationX() == 170)
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        }else
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", -170);
        }

        animation.setDuration(300);
        animation.start();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.alarm_card, parent, false);

        TextView textViewTime = (TextView) rowView.findViewById(R.id.time);
        TextView textViewDays = (TextView) rowView.findViewById(R.id.days);

        LinearLayout backLayout = (LinearLayout)rowView.findViewById(R.id.backLayout);
        LinearLayout buttonEdit = (LinearLayout)rowView.findViewById(R.id.butotn_edit);
        LinearLayout buttonDelete = (LinearLayout)rowView.findViewById(R.id.butotn_delete);
        LinearLayout frontView = (LinearLayout)rowView.findViewById(R.id.frontCard);

        buttonDelete.setTag(position);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos  = (int)v.getTag();
                values.remove(pos);
                AlarmListAdapter.this.notifyDataSetChanged();
            }
        });

        buttonEdit.setTag(position);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                if(callback != null)
                {
                    callback.onItemClicked(pos);
                }
            }
        });

        frontView.setOnTouchListener(new OnSwipeTouchListener(context)
        {
            public void onSwipeRight()
            {
                MoveToRight(frontView);
            }

            public void onSwipeLeft()
            {
                MoveToLeft(frontView);
            }
        });

        String alarmTime = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(values.get(position).millisAfterMidnight),
                TimeUnit.MILLISECONDS.toMinutes(values.get(position).millisAfterMidnight) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(values.get(position).millisAfterMidnight)));

        textViewTime.setText(alarmTime);
        textViewDays.setText("Mon, Tue, Wed, Thu, Fri");


        // Изменение иконки для Windows и iPhone

        return rowView;
    }
}