package com.example.ehotel.support;

import android.animation.ObjectAnimator;
import android.graphics.Canvas;
import android.view.View;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class SimpleItemTouchHelperCallbackForKostyl extends ItemTouchHelper.Callback {

    private final AlarmRecyclerAdapter mAdapter;

    AlarmRecyclerAdapter.AlarmViewHolder swipedViewHolder;

    public SimpleItemTouchHelperCallbackForKostyl(AlarmRecyclerAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return false;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = 0;
        int swipeFlags = 0;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

        return false;
    }

    @Override public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        swipedViewHolder = (AlarmRecyclerAdapter.AlarmViewHolder) viewHolder;
        mAdapter.notifyDataSetChanged();
    }


}
