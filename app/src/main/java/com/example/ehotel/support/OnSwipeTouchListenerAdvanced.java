package com.example.ehotel.support;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnTouchListener;


public class OnSwipeTouchListenerAdvanced implements OnTouchListener{


    boolean checkIfStartedTouch;
    boolean checkIfSwiping;
    boolean checkIfScrolling;
    private final GestureDetector gestureDetector = new GestureDetector(new GestureListener());
    private MotionEvent currentEvent;

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        currentEvent = event;
        if (event.getAction()==MotionEvent.ACTION_DOWN){
            checkIfStartedTouch = true;
        } else if (event.getAction()==MotionEvent.ACTION_UP){
            if (checkIfSwiping){
                onSwipeEnd(event);
            }
            checkIfStartedTouch = false;
            checkIfScrolling = false;
            checkIfSwiping = false;
        } else {
            if (checkIfSwiping){
                //System.out.println("swipe2");
                onSwipe(event);
                return false;
            } if (checkIfScrolling){
                //System.out.println("scroll2");
                onScroll(event);
                return false;
            }
        }

        return gestureDetector.onTouchEvent(event);
    }

    public void onSwipe(MotionEvent motionEvent) {}
    public void onScroll(MotionEvent motionEvent) {}
    public void onSwipeEnd(MotionEvent motionEvent) {}
    public void onSwipeStart(MotionEvent motionEvent) {}

    public enum Direction {
        up,
        down,
        left,
        right;

        /**
         * Returns a direction given an angle.
         * Directions are defined as follows:
         * <p/>
         * 0 is on right side middle (east direction)
         * angle is anticlockwise, 90 is at north pole
         *
         * @param angle an angle from 0 to 360 - e
         * @return the direction of an angle
         */
        public static Direction get(double angle) {
            System.out.println("Angle: "+angle);
            if (inRange(angle, 50, 130)) {
                System.out.println("UPPPPPPPPPP");
                return Direction.up;
            } else if (inRange(angle, 0, 50) || inRange(angle, 310, 360)) {
                System.out.println("RIGHT");
                return Direction.right;
            } else if (inRange(angle, 240, 310)) {
                System.out.println("DOWN");
                return Direction.down;
            } else {
                System.out.println("LEFTT");
                return Direction.left;
            }

        }//i think I have finally figured out an android coding UI problem after 3 weeks. xml files might work for web development but designing decent UIs on a dedicated device using xml files is a nightmare. The fragmentation of device different resolutions makes things even harder.

        /**
         * @param angle an angle
         * @param init  the initial bound
         * @param end   the final bound
         * @return returns true if the given angle is in the interval [init, end).
         */
        private static boolean inRange(double angle, float init, float end) {
            return (angle >= init) && (angle < end);
        }
    }

    private final class GestureListener extends SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        /*@Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {



            System.out.println("flingggg");
            float x1 = e1.getX();
            float y1 = e1.getY();

            float x2 = e2.getX();
            float y2 = e2.getY();

            Direction direction = getDirection(x1, y1, x2, y2);
            return onSwipe(direction);
        }*/

        @Override
        public boolean onScroll(MotionEvent e1,
                                MotionEvent e2,
                                float distanceX,
                                float distanceY) {

            // Grab two events located on the plane at e1=(x1, y1) and e2=(x2, y2)
            // Let e1 be the initial event
            // e2 can be located at 4 different positions, consider the following diagram
            // (Assume that lines are separated by 90 degrees.)
            //
            //
            //         \ A  /
            //          \  /
            //       D   e1   B
            //          /  \
            //         / C  \
            //
            // So if (x2,y2) falls in region:
            //  A => it's an UP swipe
            //  B => it's a RIGHT swipe
            //  C => it's a DOWN swipe
            //  D => it's a LEFT swipe
            //

            System.out.println("onscrollllll");
            float x1 = e1.getX();
            float y1 = e1.getY();

            float x2 = e2.getX();
            float y2 = e2.getY();

            Direction direction = getDirection(x1, y1, x2, y2);
            return onSwipe(direction);


        }

        public boolean onSwipe(Direction direction) {

            if (direction == Direction.left || direction == Direction.right) {
                System.out.println("swipe");
                if (!checkIfSwiping) {
                    checkIfSwiping = true;
                    onSwipeStart(currentEvent);
                }
                return false;
            }
            else {
                System.out.println("scroll");
                checkIfScrolling=true;
                return true;
            }
        }

        /**
         * Given two points in the plane p1=(x1, x2) and p2=(y1, y1), this method
         * returns the direction that an arrow pointing from p1 to p2 would have.
         *
         * @param x1 the x position of the first point
         * @param y1 the y position of the first point
         * @param x2 the x position of the second point
         * @param y2 the y position of the second point
         * @return the direction
         */
        public Direction getDirection(float x1, float y1, float x2, float y2) {
            double angle = getAngle(x1, y1, x2, y2);
            return Direction.get(angle);
        }

        /**
         * Finds the angle between two points in the plane (x1,y1) and (x2, y2)
         * The angle is measured with 0/360 being the X-axis to the right, angles
         * increase counter clockwise.
         *
         * @param x1 the x position of the first point
         * @param y1 the y position of the first point
         * @param x2 the x position of the second point
         * @param y2 the y position of the second point
         * @return the angle between two points
         */
        public double getAngle(float x1, float y1, float x2, float y2) {

            double rad = Math.atan2(y1 - y2, x2 - x1) + Math.PI;
            return (rad * 180 / Math.PI + 180) % 360;
        }



    }
}