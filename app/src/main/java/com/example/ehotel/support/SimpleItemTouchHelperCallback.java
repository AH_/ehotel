package com.example.ehotel.support;

import android.animation.ObjectAnimator;
import android.graphics.Canvas;
import android.view.View;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ehotel.R;

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final AlarmRecyclerAdapter mAdapter;

    AlarmRecyclerAdapter.AlarmViewHolder swipedViewHolder;

    public SimpleItemTouchHelperCallback(AlarmRecyclerAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return false;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = 0;
        int swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

        return false;
    }

    @Override public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        swipedViewHolder = (AlarmRecyclerAdapter.AlarmViewHolder) viewHolder;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            AlarmRecyclerAdapter.AlarmViewHolder myViewHolder = (AlarmRecyclerAdapter.AlarmViewHolder) viewHolder;
            if(dX>0)
            {
                if(((AlarmRecyclerAdapter.AlarmViewHolder) viewHolder).frontView.getTranslationX()<0)
                {
                    undo();
                }else
                {
                    getDefaultUIUtil().onDraw(c, recyclerView, myViewHolder.frontView, dX/4, dY, actionState, isCurrentlyActive);
                }

            }

        if(dX<0)
        {
            if(((AlarmRecyclerAdapter.AlarmViewHolder) viewHolder).frontView.getTranslationX()>0)
            {
                undo();
            }else
            {
                getDefaultUIUtil().onDraw(c, recyclerView, myViewHolder.frontView, dX/4, dY, actionState, isCurrentlyActive);
            }

        }
    }


    @Override public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            AlarmRecyclerAdapter.AlarmViewHolder myViewHolder = (AlarmRecyclerAdapter.AlarmViewHolder) viewHolder;
            getDefaultUIUtil().onDrawOver(c, recyclerView, myViewHolder.frontView, dX/4, dY, actionState, isCurrentlyActive);
    }


    @Override public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            AlarmRecyclerAdapter.AlarmViewHolder myViewHolder = (AlarmRecyclerAdapter.AlarmViewHolder) viewHolder;
            getDefaultUIUtil().onSelected(myViewHolder.frontView);
        }
    }

    void undo() {
        if (swipedViewHolder != null) {
            getDefaultUIUtil().clearView(swipedViewHolder.frontView);
            mAdapter.notifyDataSetChanged();
            swipedViewHolder = null;
        }
    }

    public void MoveToRight(View view)
    {
        ObjectAnimator animation;

        if(view.getTranslationX() == -170)
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        }else
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 170);
        }

        animation.setDuration(400);
        animation.start();
    }

    public void MoveToLeft(View view)
    {
        ObjectAnimator animation;

        if(view.getTranslationX() == 170)
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        }else
        {
            animation = ObjectAnimator.ofFloat(view, "translationX", -170);
        }

        animation.setDuration(300);
        animation.start();
    }
}
