package com.example.ehotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ehotel.food.RestOrLobbyActivity;
import com.example.ehotel.room.RoomTopMenuActivity;
import com.example.ehotel.support.ActivityHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MenuMain extends AppCompatActivity {

    TextView textViewTime;
    TextView textViewDate;
    TextView textViewDay;

    Handler clockHandler = new Handler();

    Runnable updateClock = new Runnable() {
        @Override
        public void run() {
            textViewTime.setText(new SimpleDateFormat("HH:mm", Globals.currentLocale).format(new Date()));
            textViewDate.setText(new SimpleDateFormat("dd MMMM yyyy", Globals.currentLocale).format(new Date()));
            textViewDay.setText(new SimpleDateFormat("EEEE", Globals.currentLocale).format(new Date()));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_main);

        ActivityHelper.hideSystemUI(this);

        textViewTime = (TextView)findViewById(R.id.textView_time);
        textViewDate = (TextView)findViewById(R.id.textView_date);
        textViewDay = (TextView)findViewById(R.id.textView_day);

        clockHandler.postDelayed(updateClock, 1000);

        Intent intent = getIntent();
        intent.getExtras();

        if(intent.hasExtra("orderStatus"))
        {
            Toast.makeText(MenuMain.this, "Your order was sent to restaurant.", Toast.LENGTH_LONG).show();
        }
    }

    public void onClickBack(View v)
    {
        super.onBackPressed();
    }

    public void onClickBtnFood(View v)
    {
        Intent foodAcivityIntent = new Intent(getApplicationContext(), RestOrLobbyActivity.class);
        startActivity(foodAcivityIntent);
    }

    public void onClickBtnTaxi(View v)
    {
        Intent taxiActivityIntent = new Intent(getApplicationContext(), TaxiActivity.class);
        startActivity(taxiActivityIntent);
    }

    public void onClickBtnRoom(View v)
    {
        Intent roomActivityIntent = new Intent(getApplicationContext(), RoomTopMenuActivity.class);
        startActivity(roomActivityIntent);
    }
}
