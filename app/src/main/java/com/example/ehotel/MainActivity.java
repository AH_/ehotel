package com.example.ehotel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    String lang = "en";
    Button nextButton, ukButotn, deButton, csButton, ruButton, zhButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Globals.alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Globals.currentLocale = getResources().getConfiguration().locale;

        ukButotn = (Button) findViewById(R.id.button_flag_uk);
        deButton = (Button) findViewById(R.id.button_flag_de);
        csButton = (Button) findViewById(R.id.button_flag_cs);
        ruButton = (Button) findViewById(R.id.button_flag_ru);
        zhButton = (Button) findViewById(R.id.button_flag_zh);

        LocaleHelper.setLocale(MainActivity.this, lang);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
            this.getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });

        Log.d("newToken", this.getPreferences(Context.MODE_PRIVATE).getString("fb", "empty :("));


        FirebaseMessaging.getInstance().subscribeToTopic("upd")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.e("topic","subscribed");
                    }
                });

        //TODO store alarms in memory


        //Alarm.removeAll(getApplicationContext());
    }

    public void onClickBtnUk(View v)
    {
        lang = "en";
        LocaleHelper.setLocale(MainActivity.this, lang);
        Intent mainMenuAcivityIntent = new Intent(getApplicationContext(), MenuMain.class);
        startActivity(mainMenuAcivityIntent);
    }

    public void onClickBtnDe(View v)
    {
        lang = "de";
        LocaleHelper.setLocale(MainActivity.this, lang);
        Intent mainMenuAcivityIntent = new Intent(getApplicationContext(), MenuMain.class);
        startActivity(mainMenuAcivityIntent);
    }

    public void onClickBtnCs(View v)
    {
        lang = "cs";
        LocaleHelper.setLocale(MainActivity.this, lang);
        Intent mainMenuAcivityIntent = new Intent(getApplicationContext(), MenuMain.class);
        startActivity(mainMenuAcivityIntent);
    }

    public void onClickBtnRu(View v)
    {
        lang = "ru";
        LocaleHelper.setLocale(MainActivity.this, lang);
        Intent mainMenuAcivityIntent = new Intent(getApplicationContext(), MenuMain.class);
        startActivity(mainMenuAcivityIntent);
    }

    public void onClickBtnZh(View v)
    {
        lang = "fr";
        LocaleHelper.setLocale(MainActivity.this, lang);
        Intent mainMenuAcivityIntent = new Intent(getApplicationContext(), MenuMain.class);
        startActivity(mainMenuAcivityIntent);
    }
}
