package com.example.ehotel.room;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ehotel.Alarm;
import com.example.ehotel.Globals;
import com.example.ehotel.R;
import com.example.ehotel.food.Bill;
import com.example.ehotel.food.BillAdapter;
import com.example.ehotel.services.AlarmHandler;
import com.example.ehotel.support.AlarmListAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class AlarmClockEditActivity extends AppCompatActivity {

    BillAdapter adapter;
    Intent intent;
    Integer position;
    Alarm alarm;
    AlarmManager alarmManager;

    int hour;
    int minute;

    RecyclerView recyclerHours;
    RecyclerView recyclerMinutes;

    CheckedTextView checkboxMon;
    CheckedTextView checkboxTue;
    CheckedTextView checkboxWed;
    CheckedTextView checkboxThu;
    CheckedTextView checkboxFri;
    CheckedTextView checkboxSat;
    CheckedTextView checkboxSun;

    public class HourViewHolder extends RecyclerView.ViewHolder
    {
        private TextView textViewHour;

        public HourViewHolder(View v)
        {
            super(v);
            textViewHour = (TextView)v.findViewById(R.id.textView_hours);
        }

        public void bindHours(String hour)
        {
            textViewHour.setText(hour);
        }
    }
    private class TimePickerHoursAdapter extends RecyclerView.Adapter<AlarmClockEditActivity.HourViewHolder>
    {
        private List<String> mHours;

        public  TimePickerHoursAdapter(List<String> hours)
        {
            mHours = hours;
        }

        @NonNull
        @Override
        public AlarmClockEditActivity.HourViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.time_picker_hours, parent, false);
            return new AlarmClockEditActivity.HourViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AlarmClockEditActivity.HourViewHolder hourViewHolder, int position) {
            String hour = mHours.get(position);
            hourViewHolder.bindHours(hour);
        }

        @Override
        public int getItemCount() {
            return mHours.size();
            //return mHours == null ? 0 : mHours.size() * 2;
        }

    }
    public class MinutesViewHolder extends RecyclerView.ViewHolder
    {
        private TextView textViewMinute;

        public MinutesViewHolder(View v)
        {
            super(v);
            textViewMinute = (TextView)v.findViewById(R.id.textView_minutes);
        }

        public void bindMinutes(String minute)
        {
            textViewMinute.setText(minute);
        }
    }
    private class TimePickerMinutesAdapter extends RecyclerView.Adapter<AlarmClockEditActivity.MinutesViewHolder>
    {
        private  List<String> mMinutes;

        public  TimePickerMinutesAdapter(List<String> minutes)
        {
            mMinutes = minutes;
        }

        @NonNull
        @Override
        public AlarmClockEditActivity.MinutesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.time_picker_minutes, parent, false);
            return new AlarmClockEditActivity.MinutesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AlarmClockEditActivity.MinutesViewHolder minuteViewHolder, int position) {
            String minute = mMinutes.get(position);
            minuteViewHolder.bindMinutes(minute);
        }

        @Override
        public int getItemCount() {
            return mMinutes.size();

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_alarm);

        intent = getIntent();
        position = intent.getIntExtra("position", -1);
        if(position != -1)
        {
            alarm = Globals.alarmList.get(position);
        }

        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        recyclerHours = (RecyclerView)findViewById(R.id.recyclerView_hours);
        recyclerMinutes = (RecyclerView)findViewById(R.id.recyclerView_minutes);

        checkboxMon = (CheckedTextView)findViewById(R.id.checkbox_mon);
        checkboxTue = (CheckedTextView)findViewById(R.id.checkbox_tue);
        checkboxWed = (CheckedTextView)findViewById(R.id.checkbox_wed);
        checkboxThu = (CheckedTextView)findViewById(R.id.checkbox_thu);
        checkboxFri = (CheckedTextView)findViewById(R.id.checkbox_fri);
        checkboxSat = (CheckedTextView)findViewById(R.id.checkbox_sat);
        checkboxSun = (CheckedTextView)findViewById(R.id.checkbox_sun);

        checkboxMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });

        checkboxTue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });

        checkboxWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });

        checkboxThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });

        checkboxFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });

        checkboxSat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });

        checkboxSun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView)v).toggle();
            }
        });



        //From Bill.class DIRTY
        AlarmClockEditActivity.TimePickerHoursAdapter hoursAdapter;
        AlarmClockEditActivity.TimePickerMinutesAdapter minutesAdapter;
        //Hours
        ArrayList<String> hours = new ArrayList<>();
        hours.add("22");
        hours.add("23");
        hours.add("Right");
        for(int i = 0; i<24; i++)
        {
            hours.add(String.format("%02d", i));
        }
        for(int i = 0; i<24; i++)
        {
            hours.add(String.format("%02d", i));
        }
        hours.add("00");
        hours.add("01");


        //Minutes
        ArrayList<String> minutes = new ArrayList<>();
        minutes.add("58");
        minutes.add("59");
        for(int i = 0; i<60; i++)
        {
            minutes.add(String.format("%02d", i));
        }
        for(int i = 0; i<60; i++)
        {
            minutes.add(String.format("%02d", i));
        }
        minutes.add("00");
        minutes.add("01");

        LinearLayoutManager linearLayoutManagerHours = new LinearLayoutManager(this);
        recyclerHours.setLayoutManager(linearLayoutManagerHours);
        linearLayoutManagerHours.scrollToPosition(25); //Preset recycler view
        recyclerHours.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstItemVisible = linearLayoutManagerHours.findFirstVisibleItemPosition();
                int lastItemVisible = linearLayoutManagerHours.findLastVisibleItemPosition();
                hour = Integer.parseInt(hours.get(firstItemVisible +3));
                if(firstItemVisible == 0)
                {
                    recyclerView.getLayoutManager().scrollToPosition(30);
                }
                if(lastItemVisible == 54)
                {
                    recyclerView.getLayoutManager().scrollToPosition(24);
                }
            }
        });

        LinearLayoutManager linearLayoutManagerMinutes = new LinearLayoutManager(this);
        recyclerMinutes.setLayoutManager(linearLayoutManagerMinutes);
        linearLayoutManagerMinutes.scrollToPosition(61);
        recyclerMinutes.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstItemVisible = linearLayoutManagerMinutes.findFirstVisibleItemPosition();
                int lastItemVisible = linearLayoutManagerMinutes.findLastVisibleItemPosition();
                minute = Integer.parseInt(minutes.get(firstItemVisible + 3));
                if(firstItemVisible == 0)
                {
                    recyclerView.getLayoutManager().scrollToPosition(66);
                }
                if(lastItemVisible == 125)
                {
                    recyclerView.getLayoutManager().scrollToPosition(59);
                }
            }
        });

        hoursAdapter = new AlarmClockEditActivity.TimePickerHoursAdapter(hours);
        minutesAdapter = new AlarmClockEditActivity.TimePickerMinutesAdapter(minutes);

        LinearSnapHelper snapHelperHours = new LinearSnapHelper();
        LinearSnapHelper snapHelperMinutes = new LinearSnapHelper();
        snapHelperHours.attachToRecyclerView(recyclerHours);
        snapHelperMinutes.attachToRecyclerView(recyclerMinutes);

        recyclerHours.setAdapter(hoursAdapter);
        recyclerMinutes.setAdapter(minutesAdapter);

        hour = linearLayoutManagerHours.findFirstVisibleItemPosition()+3;
        minute = linearLayoutManagerMinutes.findFirstVisibleItemPosition()+3;
    }

    private Boolean[] getDays()
    {
        Boolean[] days = new Boolean[] {checkboxMon.isChecked(), checkboxTue.isChecked(), checkboxWed.isChecked(),
        checkboxThu.isChecked(), checkboxFri.isChecked(), checkboxSat.isChecked(), checkboxSun.isChecked()};

        return  days;
    }

    public void onClickBack(View v)
    {
        setResult(RESULT_CANCELED, null);
        finish();
    }

    public void onSaveButtonClick(View v)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        Date time = cal.getTime();

        long millisAfterMidnight = hour*60*60*1000 + minute*60*1000;

        if(position != -1)
        {
            //alarm.id = position;
            alarm.days = getDays();
            alarm.millisAfterMidnight = millisAfterMidnight;
            alarm.settedUp = true;

        }else
        {
            alarm = Alarm.create(millisAfterMidnight, getDays(), this);
            alarm.set(this);
        }

        setResult(RESULT_OK, null);
        finish();
    }

    public void onClearButtonClick(View v)
    {
        //alarm.removeAll(this);
    }
}
