package com.example.ehotel.room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.ehotel.Alarm;
import com.example.ehotel.Globals;
import com.example.ehotel.MenuMain;
import com.example.ehotel.R;
import com.example.ehotel.support.AlarmKostylRecyclerAdapter;
import com.example.ehotel.support.WakeLocker;

public class AlarmClockActivity extends AppCompatActivity {

    int[] colors = {0, 0x25060606, 0};
    RecyclerView recycler;
    LinearLayoutManager linearLayoutManager;
    AlarmKostylRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_clock);

        recycler = (RecyclerView)findViewById(R.id.alarm_recycler);

        linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);

        Globals.alarmList = Alarm.readFromSharedPrefs(this);
        adapter = new AlarmKostylRecyclerAdapter(Globals.alarmList, this);
        recycler.setAdapter(adapter);

        adapter.SetOnItemClickListener(new AlarmKostylRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onSwitchClick(View v, int position, boolean isChecked) {
                Alarm alarm = adapter.getItem(position);
                if(isChecked)
                {
                    alarm.set(getApplicationContext());
                }
                else
                {
                    alarm.cancel(getApplicationContext());
                }
            }

            @Override
            public void onItemClick(View v, int position) {
                Intent editAlarmActivityIntent = new Intent(getApplicationContext(), AlarmClockEditActivity.class);
                editAlarmActivityIntent.putExtra("position", position);
                startActivityForResult(editAlarmActivityIntent,position);
            }

            @Override
            public void onDeleteClick(View v, int position)
            {
                Alarm alarm = adapter.getItem(position);
                alarm.remove(getApplicationContext());
                adapter.notifyItemRemoved(position);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK)
        {
            adapter.notifyItemChanged(requestCode);
        }
    }

    public void showToast(String text)
    {
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onClickButotnAdd(View v)
    {
        Intent editAlarmActivityIntent = new Intent(getApplicationContext(), AlarmClockEditActivity.class);
        startActivityForResult(editAlarmActivityIntent,Globals.alarmList.size()+1);
    }


    public void onClickBack(View v)
    {
        super.onBackPressed();
    }


}
