package com.example.ehotel.room;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ehotel.Alarm;
import com.example.ehotel.Globals;
import com.example.ehotel.MenuMain;
import com.example.ehotel.R;
import com.example.ehotel.support.ActivityHelper;
import com.example.ehotel.support.AlarmKostylRecyclerAdapter;
import com.example.ehotel.support.WakeLocker;

import java.util.Date;

public class AlarmTriggeredActivity extends AppCompatActivity {

    Ringtone ringtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        setContentView(R.layout.activity_alarm_triggered);

        ActivityHelper.hideSystemUI(this);

        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), alarmUri);
        ringtone.play();
    }

    public void onClickButotnStop(View v)
    {
        ringtone.stop();
        Intent backToMainIntent = new Intent(getApplicationContext(), MenuMain.class);
        startActivity(backToMainIntent);
        WakeLocker.release();
        finish();
    }

    public void onClickBack(View v)
    {
        super.onBackPressed();
    }
}
