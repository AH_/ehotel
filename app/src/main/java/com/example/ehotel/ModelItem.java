package com.example.ehotel;

import java.util.ArrayList;
import java.util.List;

public class ModelItem {

    private String author;
    private  int imgId;

    public ModelItem(String author, int imgId)
    {
        this.author = author;
        this.imgId = imgId;
    }

    public int getImgId()
    {
        return  imgId;
    }

    public  String getAuthor()
    {
        return author;
    }

    public  static List<ModelItem> getFakeItems()
    {
        ArrayList<ModelItem> itemList = new ArrayList<>();
        itemList.add(new ModelItem("AH", R.drawable.placeholder));
        itemList.add(new ModelItem("one more AH", R.drawable.placeholder));
        itemList.add(new ModelItem("another AH", R.drawable.placeholder));
        itemList.add(new ModelItem("and again AH", R.drawable.placeholder));
        itemList.add(new ModelItem("AH No 5", R.drawable.placeholder));
        itemList.add(new ModelItem(" the lastAH", R.drawable.placeholder));

        return itemList;
    }
}
