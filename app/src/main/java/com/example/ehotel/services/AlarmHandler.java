package com.example.ehotel.services;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;

import com.example.ehotel.MenuMain;
import com.example.ehotel.R;
import com.example.ehotel.room.AlarmClockEditActivity;
import com.example.ehotel.room.AlarmTriggeredActivity;
import com.example.ehotel.support.WakeLocker;

import static androidx.legacy.content.WakefulBroadcastReceiver.startWakefulService;

public class AlarmHandler extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        WakeLocker.acquire(context);
        Intent triggeredAlarmClockIntent = new Intent(context, AlarmTriggeredActivity.class);
        context.startActivity(triggeredAlarmClockIntent);
    }
}
