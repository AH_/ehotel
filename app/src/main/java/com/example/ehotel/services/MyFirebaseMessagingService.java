package com.example.ehotel.services;

import android.app.Notification;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.ehotel.Alarm;
import com.example.ehotel.MainActivity;
import com.example.ehotel.R;
import com.example.ehotel.food.MenuLoader;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.Console;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG="MyFirebaseInstanceServi";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        /*
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();
        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
        manager.notify(123, notification);
        */
        if(remoteMessage.getData().get("message").equals("Set alarm pls."))
        {
            Boolean[] days = {true, true, true, true, true, true, true};
            Alarm a = Alarm.create(15600000, days, this);
            a.set(this);
        }

        MenuLoader menuLoader = new MenuLoader(getApplicationContext());
        menuLoader.deleteFromSharedPreferences("file", getApplicationContext());
        menuLoader.removeFromInternalStorage();

        menuLoader.downloadRestaurantMenuFromFirebase();
    }


}
