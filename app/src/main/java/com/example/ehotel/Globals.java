package com.example.ehotel;

import android.app.AlarmManager;
import android.app.PendingIntent;

import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

public class Globals {
    public static Locale currentLocale;
    public static ArrayList<Alarm> alarmList = new ArrayList<>();
    public static ArrayList<PendingIntent> alarmManagerList = new ArrayList<>();
    public static AlarmManager alarmManager;
}
