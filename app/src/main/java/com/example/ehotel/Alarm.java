package com.example.ehotel;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.ehotel.services.AlarmHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Alarm {

    //Alarm unit
    //alarmTime - at what time
    //days - on which days
    public int id;
    public long millisAfterMidnight;
    public Boolean [] days;
    public Boolean settedUp;

    public Alarm(int id, long millisAfterMidnight, Boolean[] days)
    {
        this.id = id;
        this.millisAfterMidnight = millisAfterMidnight;
        this.days = days;
        this.settedUp = false;
    }

    public boolean equals(Alarm alarm)
    {
        if(alarm.days == this.days && alarm.millisAfterMidnight == this.millisAfterMidnight)
        {
            return true;
        }
        return false;
    }

    public boolean isInList()
    {
        for (Alarm alarm : Globals.alarmList)
        {
            if(alarm.equals(this))
            {
                return true;
            }
        }
        return false;
    }

    public long getMillisOfNextWeekDay(int weekDay)
    {
        Calendar cal = Calendar.getInstance(); // Today, now

        if (cal.get(Calendar.DAY_OF_WEEK) != weekDay) {
            cal.add(Calendar.DAY_OF_MONTH, (weekDay + 7 - cal.get(Calendar.DAY_OF_WEEK)) % 7);
        }
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        if((cal.getTimeInMillis()+this.millisAfterMidnight)-Calendar.getInstance().getTimeInMillis() <= 0)
        {
            cal.add(Calendar.DAY_OF_MONTH, 7);
        }

        return cal.getTimeInMillis();
    }

    public static Alarm create(long millisFromMidnight, Boolean[] days, Context context)
    {
        ArrayList<Integer> ids = new ArrayList<>(); //List of all Alarms ids
        for(Alarm alarm : Globals.alarmList)    //Fetch ids
        {
            ids.add(alarm.id);
        }

        int id = 0; //Pick is for new Alarm, start from 0
        while(id<666)   //Safe counter
        {
            if(!ids.contains(id))   //Break if id isn't in list
            {
                break;
            }
            id++;   //If id is in list, try next
        }

        Alarm alarm = new Alarm(id, millisFromMidnight, days);
        if(!alarm.isInList())   //Check days && time
        {
            Globals.alarmList.add(alarm);   //Add to alarmList
            saveAlarmListToSharedPrefs(context);  //Save alarmList to SP
        }else
        {
            Log.e("AG", "Alarm:84 - Alarm is already in list");
        }
        return alarm;   //Return alarm for further manipulations
    }

    public void set(Context context)
    {
        int index = Globals.alarmList.indexOf(this);
        this.settedUp = true;
        Globals.alarmList.set(index, this);
        saveAlarmListToSharedPrefs(context);
        this.saveToAlarmManager(context);
    }

    public void cancel(Context context)
    {
        int index = Globals.alarmList.indexOf(this);
        this.settedUp = false;
        Globals.alarmList.set(index, this);
        saveAlarmListToSharedPrefs(context);

        removeFromAlarmManager(context);
    }

    public void remove (Context context)
    {
        Globals.alarmList.remove(this);
        saveAlarmListToSharedPrefs(context);
        this.removeFromAlarmManager(context);
    }

    private void saveToAlarmManager(Context c)
    {
        Intent alarmIntent = new Intent(c, AlarmHandler.class);

        if (this.days[0])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.MONDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"0"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
        if (this.days[1])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.TUESDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"1"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
        if (this.days[2])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.WEDNESDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"2"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
        if (this.days[3])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.THURSDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"3"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
        if (this.days[4])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.FRIDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"4"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
        if (this.days[5])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.SATURDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"5"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
        if (this.days[6])
        {
            long millisOfNextWeekDay = getMillisOfNextWeekDay(Calendar.SUNDAY);
            long firstTriggerTime = millisOfNextWeekDay + this.millisAfterMidnight;
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"6"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.setExact(AlarmManager.RTC_WAKEUP, firstTriggerTime, pendingIntent);
        }
    }

    private void removeFromAlarmManager(Context c)
    {
        Intent alarmIntent = new Intent(c, AlarmHandler.class);

        if (this.days[0])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"0"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
        if (this.days[1])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"1"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
        if (this.days[2])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"2"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
        if (this.days[3])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"3"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
        if (this.days[4])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"4"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
        if (this.days[5])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"5"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
        if (this.days[6])
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, Integer.parseInt(Integer.toString(this.id)+"6"), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Globals.alarmManager.cancel(pendingIntent);
        }
    }

    private static void removeAll(Context c)
    {
        Intent alarmIntent = new Intent(c, AlarmHandler.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(c, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Globals.alarmManager.cancel(pendingIntent);
    }

    private static void saveAlarmListToSharedPrefs(Context context)
    {
        Gson gson = new Gson();
        SharedPreferences settings = context.getSharedPreferences("alarms", 0);

        String alarmJson = gson.toJson(Globals.alarmList);

        SharedPreferences.Editor editor = settings.edit();
        editor.putString("alarms", alarmJson);

        editor.apply();
    }

    public static ArrayList<Alarm> readFromSharedPrefs(Context context)
    {
        SharedPreferences settings = context.getSharedPreferences("alarms", 0);
        String alarmJson = settings.getString("alarms", null);

        Gson gson = new Gson();
        ArrayList<Alarm> alarmList = new ArrayList<>();

        if(alarmJson != null)
        {
            alarmList = gson.fromJson(alarmJson, new TypeToken<List<Alarm>>(){}.getType());
        }
        return alarmList;
    }

    private static Alarm getAlarmWithMinId(ArrayList<Alarm> alarms)
    {
        Alarm alarmWithMinId = null;   //Default return value. means that AlarmList is empty.
        if(alarms.size() > 0)   //Check if AlarmList is not empty
        {
            alarmWithMinId = alarms.get(0); //If alarm list is not empty start from first element's id.
            for(Alarm alarm : alarms)   //Iterate through alarms looking fo smaller id tham previous is.
            {
                if(alarm.id < alarmWithMinId.id)
                {
                    alarmWithMinId = alarm;
                }
            }

        }
        return alarmWithMinId;
    }
}
